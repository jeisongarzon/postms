package com.controllers;
import com.exceptions.interestException;
import com.models.interest;
import com.repositories.interestRepository;
import org.springframework.web.bind.annotation.*;
@RestController
public class interestController {
    private final interestRepository InterestRepository;
    public interestController(interestRepository InterestRepository) {
        this.InterestRepository = InterestRepository;
    }
    @GetMapping("/interest/{userName}")
    interest getInterest(@PathVariable String userName){
        return InterestRepository.findById(userName).orElseThrow(() -> new interestException("No se encontro una cuenta con el username: " + userName));
    }
    @PostMapping("/interest")
    interest newInterest(@RequestBody interest Interest){
        return InterestRepository.save(Interest);
    }
}