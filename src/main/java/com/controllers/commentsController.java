package com.controllers;
import com.exceptions.interestException;
import com.models.comments;
import com.repositories.commentsRepository;
import org.springframework.web.bind.annotation.*;
@RestController
public class commentsController {
    private final commentsRepository CommentsRepository;
    public commentsController(commentsRepository CommentsRepository) {
        this.CommentsRepository = CommentsRepository;
    }
    @GetMapping("/comments/{userName}")
    comments getComments(@PathVariable String userName){
        return CommentsRepository.findById(userName).orElseThrow(() -> new interestException("No se encontro una cuenta con el username: " + userName));
    }
    @PostMapping("/comments")
    comments newComment(@RequestBody comments Comments){
        return CommentsRepository.save(Comments);
    }
}