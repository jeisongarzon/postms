package com.controllers;
import com.exceptions.interestException;
import com.models.interest;
import com.models.post;
import com.repositories.postRepository;
import org.springframework.web.bind.annotation.*;
@RestController
public class postController {
    private final postRepository PostRepository;
    public postController(postRepository PostRepository) {
        this.PostRepository = PostRepository;
    }
    @GetMapping("/post/{userName}")
    post getPost(@PathVariable String userName){
        return PostRepository.findById(userName).orElseThrow(() -> new interestException("No se encontro una cuenta con el username: " + userName));
    }
    @PostMapping("/post")
    post newPost(@RequestBody post Post){
        return PostRepository.save(Post);
    }
}