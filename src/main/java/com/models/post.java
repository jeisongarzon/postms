package com.models;
import org.springframework.data.annotation.Id;

public class post {
@Id
private String id;
private String titlePost;
private String descriptionPost;
private String imagePost;
private String datePost;
private String idUserFK;
private String userName;
private String nameCategory;

public post (String id, String titlePost, String descriptionPost, String imagePost, String datePost, String idUserFK, String username,String nameCategory){
    this.id = id;
    this.titlePost = titlePost;
    this.descriptionPost = descriptionPost;
    this.imagePost = imagePost;
    this.datePost = datePost;
    this.idUserFK = idUserFK;
    this.userName = userName;
    this.nameCategory = nameCategory;
}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitlePost (){
        return titlePost;
    }
    public void setTitlePost(String titlePost){
        this.titlePost = titlePost;
    }
    
    public String getDescriptionPost(){
        return descriptionPost;
    }
    public void setDescriptionPost(String descriptionPost){
        this.descriptionPost = descriptionPost;
    }

    public String getImagePost(){
        return imagePost;
    }
    public void setImagePost(String imagePost){
        this.imagePost = imagePost;
    }

    public String getDatePost(){
        return datePost;
    }
    public void setDatePost(String datePost){
        this.datePost = datePost;
    }

    public String getIdUserFK() {
        return idUserFK;
    }
    public void setIdUserFK(String idUserFK) {
        this.idUserFK = idUserFK;
    }

    public String getUserName(){
        return userName;
    }
    public void setUserName(String userName){
        this.userName = userName;
    }

    public String getNameCategory(){
        return nameCategory;
    }
    public void setNameCategory(String nameCategory){
        this.nameCategory = nameCategory;
    }
}