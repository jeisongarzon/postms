package com.models;
import org.springframework.data.annotation.Id;
import java.util.Date;
public class comments {
@Id
private String id;
private String commentContent;
private String idUserFK;
private String userName;
private String idPostFK;
private Date dateComment;


public comments(String id, String commentContent, String idUserFK, String userName, String idPostFK,Date dateComment){

    this.id= id;
    this.commentContent = commentContent;
    this.idUserFK = idUserFK;
    this.userName = userName;
    this.idPostFK = idPostFK;
    this.dateComment= dateComment;



}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getIdUserFK() {
        return idUserFK;
    }

    public void setIdUserFK(String idUserFK) {
        this.idUserFK = idUserFK;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIdPostFK() {
        return idPostFK;
    }

    public void setIdPostFK(String idPostFK) {
        this.idPostFK = idPostFK;
    }

    public Date getDateComment() {
        return dateComment;
    }

    public void setDateComment(Date dateComment) {
        this.dateComment = dateComment;
    }}
