package com.models;
import org.springframework.data.annotation.Id;

public class interest {
    @Id
private String id;
private String idUserFK;
private String idPostFK;
private String interestDate;
private String userName;


public interest(String id, String idUserFK, String idPostFK, String interestDate, String userName){
    this.id = id;
    this.idUserFK = idUserFK;
    this.idPostFK = idPostFK;
    this.interestDate = interestDate;
    this.userName = userName;
}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUserFK() {
        return idUserFK;
    }

    public void setIdUserFK(String idUserFK) {
        this.idUserFK = idUserFK;
    }

    public String getIdPostFK() {
        return idPostFK;
    }

    public void setIdPostFK(String idPostFK) {
        this.idPostFK = idPostFK;
    }

    public String getInterestDate() {
        return interestDate;
    }

    public void setInterestDate(String interestDate) {
        this.interestDate = interestDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}